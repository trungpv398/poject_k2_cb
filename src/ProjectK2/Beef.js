import React, { Component } from 'react';
import { connect } from "react-redux";

class Beef extends Component {
    renderBeef = () =>{     
        let {burger} = this.props;
        let content =[];
        let proBurger = burger["beef"];
        for(let i=0;i<proBurger;i++)
        {
            content.push(<div className="beef"></div>)
        }
        return    content;
    }
    render() {
        return (
            <div className="row">
                {this.renderBeef()}               
            </div>
        )
    }
}

const mapStateToProps =(state)=>{
    return {
        burger:state.BurgerReducer.burger
    }
}

export default connect(mapStateToProps,null)(Beef)

