import React, { Component } from 'react'
import { connect } from "react-redux";

class Cheese extends Component {
    renderCheese = () =>{
        // console.log(this.props.burger);
        let {burger} = this.props;
        let content = []
        let proBurger = burger["cheese"];
        for(let i=0;i<proBurger;i++)
        {
            content.push( <div className="cheese"></div>)
        }

        return   content;
    }
    render() {
        return (
            <div className="row">
                {this.renderCheese()}              
            </div>
        )
    }
}

const mapStateToProps =(state)=>{
    return{
        burger:state.BurgerReducer.burger
    }
}

export default connect(mapStateToProps,null)(Cheese)

