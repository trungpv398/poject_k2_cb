import React, { Component } from 'react';
import Beef from './Beef';
import Cheese from './Cheese';
import './ProjectK2.css'
import Salad from './Salad';
import dataMenu from '../Data/menu.json';
import { connect } from "react-redux";
import {CongBurgerAction,TruBurgerAction  } from "../redux/actions/BurgerActions";
class ProjectK2 extends Component {
    
    
    
    renderMenuItem=()=>{
        //console.log(this.props.burger);
        let {burger} = this.props;
        let {menu} = this.props;
        let content = []
        let i =0;
        for(let proBurger in burger)
        {
            for(let proMenu in menu)
            {
                if (proBurger === proMenu) {
                    content.push( <tr key={i+1+proBurger}>
                        <td>{proBurger}</td>
                        <td><button onClick={()=>{
                            this.props.dispatch(CongBurgerAction(proBurger))
                        }} style={{backgroundColor:"green",color:"#fff"}}>+</button> <input type="text"  value={burger[proBurger]} style={{width:"40px",textAlign:"center"}}/> <button onClick={()=>{
                            this.props.dispatch(TruBurgerAction(proBurger))
                        }} style={{backgroundColor:"red",color:"#fff"}}>-</button></td>
                        <td>{menu[proMenu]}</td>
                        <td>{menu[proMenu] * burger[proBurger]}</td> 
                    </tr>
                    )
                }
            }
        }

              
            return content;
         
      
    }
    

    render() {
        
        return (
            <div className="container">
                <div className="row">
                    <div className="col-7 mt-3 ">
                        <div className="col-12 text-center"><h1 style={{color:"green"}}>Bài tập burger cybersoft</h1></div>
                        <div className="col-12 text-center mt-4"><h3 style={{color:"#b51010"}}>Bánh burger của bạn</h3></div>
                        
                           
                            <div className="offset-3 col-6 banhBugerTop mt-4" ></div>
                            <div className="offset-3 col-6 mt-2 mb-2 ">
                                <Salad/>
                                <Cheese/>
                                <Beef/>
                            </div>
                            <div className="offset-3 col-6 banhBugerBottom " ></div>
                        
                       
                    </div>
                   
                    <div className="col-5" style={{marginTop:"90px"}}>
                        <div className=" text-center">
                            <h3 style={{color:"green"}}>Chọn thức ăn</h3>
                        </div>
                        <div className="col-12">
                            <table >  
                                <thead style={{height:"100px"}}>               
                                    <tr className="mt-1 mb-1" style={{borderTop:"1px solid gray",borderBottom:"1px solid gray"}}>
                                        <th className="mt-1 mb-1">Thức ăn</th>
                                        <th></th>
                                        <th>Đơn giá</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    {this.renderMenuItem()}
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colSpan="2"></td>
                                        <td>Tổng cộng</td>
                                        <td>{this.props.totalMoney}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                                              
                    </div>
                </div>
               
              
            </div>
        )
    }
}

const mapStateToProps = state =>{
    return {
        burger:state.BurgerReducer.burger,
        menu:state.BurgerReducer.menu,
        totalMoney:state.BurgerReducer.total
    }
}
export default connect(mapStateToProps,null)(ProjectK2)