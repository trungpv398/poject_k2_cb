import React, { Component } from 'react';
import dataMenu from '../Data/menu.json';
import {connect} from 'react-redux'

class Salad extends Component {
   
    renderSalad = () =>{
      let {burger} = this.props;
      let content = []
      let probu = burger["salad"];
    //   for(let proBurger in burger)
    //   {
    //     if(proBurger==="salad")
    //     {
    //         for(let i=0;i<burger[proBurger];i++)
    //         {
    //             content.push(<div key={i} className="salad"></div>)
    //         }
    //     }
    //   }
      for(let i=0;i<probu;i++)
      {
          content.push(<div key={i} className="salad"></div>)
      }      
        return  content;       
    }
    render() {
        return (
            <div className="row" style={{padding:"0 0 -30px -30px"}}>
                {this.renderSalad()}                          
            </div>
        )
    }
}
const mapStateToProps = (state)=>{
    return {
        burger : state.BurgerReducer.burger

    }
}

export default connect(mapStateToProps,null)(Salad)
