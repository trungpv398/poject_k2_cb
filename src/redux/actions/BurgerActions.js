import { CONG,TRU } from "../types/BurgerType";
export const CongBurgerAction = (propBuger)=>{
    return {
        type:CONG,
        propBuger
    }
}

export const TruBurgerAction = (propBuger) =>{
    return{
        type:TRU,
        propBuger
    }
}