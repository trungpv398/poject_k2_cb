import {CONG,TRU} from "../types/BurgerType";
const burgerState = {
    burger:{salad:1,cheese:1,beef:1},
    menu:{
        salad:10,
        cheese:20,
        beef:55
    },
    total:85
}

export const BurgerReducer = (state=burgerState,action)=>{
    switch (action.type) {
        case CONG:{
            // console.log(action);
           let burgerStateUpdate = {...state.burger};
           burgerStateUpdate[action.propBuger]+=1;
           if(burgerStateUpdate[action.propBuger]<0){
               burgerStateUpdate[action.propBuger]=0;
           }
           state.burger = burgerStateUpdate;

           let menu = {...state.menu}
           let totalUpdate = 0;
           for(let itemBurger in burgerStateUpdate)
           {
                for(let itemMenu in menu)
                {
                    if(itemBurger === itemMenu)
                    {
                        if(burgerStateUpdate[itemBurger]>0)
                        {
                            totalUpdate += burgerStateUpdate[itemBurger] * menu[itemMenu];
                        }else{
                            totalUpdate+=0;
                        }
                    }
                }
           }
           state.total = totalUpdate;
            return {...state}

        }
        case TRU:{
            let burgerStateUpdate = {...state.burger};
            burgerStateUpdate[action.propBuger]-=1;
            if(burgerStateUpdate[action.propBuger]<0){
                burgerStateUpdate[action.propBuger]=0;
            }
            state.burger = burgerStateUpdate;
 
            let menu = {...state.menu}
            let totalUpdate = 0;
            for(let itemBurger in burgerStateUpdate)
            {
                 for(let itemMenu in menu)
                 {
                     if(itemBurger === itemMenu)
                     {
                         if(burgerStateUpdate[itemBurger]>0)
                         {
                             totalUpdate += burgerStateUpdate[itemBurger] * menu[itemMenu];
                         }else{
                             totalUpdate+=0;
                         }
                     }
                 }
            }
            state.total = totalUpdate;
            return {...state}

        }
            
    
        default:
            return {...state}
    }
   
}